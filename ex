#include "pch.h"
#include <string>
#include <iostream>

using namespace std;

class Exams;
class Faculty;
class Abiturient;
class Teacher;




class Exams {
	string Exam1;
	string Exam2;
	int AssessmentExam1;
	int AssessmentExam2;
public:
	Exams(string exam1, string exam2, int assess1, int assess2) { Exam1 = exam1; Exam2 = exam2; AssessmentExam1 = assess1; AssessmentExam2 = assess2; }
	string getExam1() { return Exam1; }
	string getExam2() { return Exam2; }
	int getAsses1() { return AssessmentExam1; }
	int getAsses2() { return AssessmentExam2; }
	void setAssess1(int a) { AssessmentExam1 = a;}
	void setAssess2(int a) { AssessmentExam2 = a; }
};

class Faculty {
	string name;
	Exams* exams;
public:
	Faculty() { name = ""; exams = new Exams("NONE", "NONE", 2, 2); }
	Faculty(string name, string exam1, string exam2) { this->name = name; exams = new Exams(exam1, exam2, 2, 2); }
	string getName() { return name; }
	Exams* getExamsAdress() { return exams; }
};

class Abiturient {
	string fio;
	Faculty* faculty;
public:
	Abiturient() { fio = "Alexej"; faculty = new Faculty("NONE", "NONE", "NONE"); }
	Abiturient(string fio) { this->fio = fio; faculty = new Faculty("NONE", "NONE", "NONE"); }
	Faculty* getFacultyAddress() { return faculty; }
	string getName() { return fio; }
	void enroll(Faculty* FacultyName) { faculty = FacultyName; cout << fio << " is enrolled!\n"; }
	void print() { cout << fio << " is applying for " << faculty->getName()<<" faculty and passing "<<faculty->getExamsAdress()->getExam1()<<" and "<< faculty->getExamsAdress()->getExam2()<<".\n";
	cout << fio << " got " << faculty->getExamsAdress()->getAsses1() << " for " << faculty->getExamsAdress()->getExam1() << " and " << faculty->getExamsAdress()->getAsses2() << " for " << faculty->getExamsAdress()->getExam2() << endl;
	}
	void result() {
		float itog = (faculty->getExamsAdress()->getAsses1() + faculty->getExamsAdress()->getAsses2()) / 2;
		cout << "The " << fio << "'s mean assessment is " << itog<<".\n"; 
		if (itog >= 4)
			cout << fio << " is enrolled. Congratulations!!!\n";
		else
			cout << itog << " is too little to be enrolled at our university\n";
	}
};

class Teacher {
	string fio;
	string Exam;
public:
	Teacher() { fio = "Popov", Exam = ""; }
	Teacher(string fio, string exam) { this->fio = fio; Exam = exam; }
	void assessStudent(Abiturient student, int assessment) { 
		if (Exam == student.getFacultyAddress()->getExamsAdress()->getExam1()) {
			student.getFacultyAddress()->getExamsAdress()->setAssess1(assessment);
			cout << fio << " assessed " << Exam << " as " << assessment << endl;
		}
		else if (Exam == student.getFacultyAddress()->getExamsAdress()->getExam2()) {
			student.getFacultyAddress()->getExamsAdress()->setAssess2(assessment);
			cout << fio << " assessed " << Exam << " as " << assessment << endl;
		}
		else
			cout << "This teacher cannot assess this student!\n";
	}
	
};



int main() {

	string name, subject, subject1, subject2, decision;

	cout << "Write students's name: ";
	cin >> name;
	Abiturient student(name);

	cout << "\nWrite faculty name: "; cin >> name;
	cout << "Write the first exam: "; cin >> subject1;
	cout << "Write the second exam: "; cin >> subject2;
	Faculty SomeFaculty(name, subject1, subject2);


	cout << "\n\nWrite teachers's 1 name: "; cin >> name;
	cout << "Write teachers's 1 subject: "; cin >> subject;
	Teacher first(name, subject);
	cout << "\nWrite teachers's 2 name: "; cin >> name;
	cout << "Write teachers's 2 subject: "; cin >> subject;
	Teacher second(name, subject);

	cout << "\n\nDo you want to enroll " << student.getName() << " to " << SomeFaculty.getName() << " faculty? (Yes/No): ";
	cin >> decision;
	if (decision == "Yes") {
		student.enroll(&SomeFaculty);
		first.assessStudent(student, 3);
		second.assessStudent(student, 5);
		cout << endl << endl;
		student.print();
		student.result();
	}

	return 0;

}